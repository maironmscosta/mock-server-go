package model

import (
	"encoding/json"
)

type Mock struct {
	Name    string            `json:"name" yaml:"name"`
	Version string            `json:"version" yaml:"version"`
	Actions map[string]Action `json:"actions" yaml:"actions"`
}

type Action struct {
	HoldConnectionAtLeast int                       `json:"hold_connection_at_least" yaml:"hold_connection_at_least"`
	AcceptRequestMethod   map[string]map[string]any `json:"accept_request_method" yaml:"accept_request_method"`
	Response              Response                  `json:"response" yaml:"response"`
}

type Response struct {
	StatusCode int               `json:"status_code" yaml:"status_code"`
	Headers    map[string]string `json:"headers" yaml:"headers"`
	Body       map[string]any    `json:"body" yaml:"body"`
}

func (response *Response) GetBody() ([]byte, error) {
	return json.Marshal(response.Body)
}
