package environment

import "time"

type setting struct {
	Server struct {
		Context string `envconfig:"SERVER_CONTEXT" default:"mock-server"`
		Port    string `envconfig:"PORT" default:"5001" required:"true" ignored:"false"`
	}

	MockService struct {
		RefreshInterval time.Duration `envconfig:"REFRESH_INTERVAL" default:"5m" required:"true" ignored:"false"`
	}
}

var Setting setting
