
# Mock Server Go

Este projeto tem o intuito de ajudar no desenvolvendo realizando mock de 
retornos de APIs.

## Uso

Nesta versão é possível adicionar mocks para realizar testes de duas formas,
adicionando arquivos ```.json``` ou ```.yaml``` (```.yml```) 
ou realizar um *request* com o mock que será utilizado como exemplo.

### Adicionando mock por arquivo

Na pasta *./mocks* (que se encontra na raíz no projeto) 
adicionar arquivos ```.json``` ou ```.yaml``` (```.yml```). Os mocks neste
caso serão carregados somente quando a aplicação for inicializada. 

#### Exemplo JSON

```
{
  "name": "mock-server-go-json",
  "version": "1.0.0",
  "actions": {
    "action_example": {
      "hold_connection_at_least": 2,
      "accept_request_method": {
        "GET": true,
        "POST": false
      },
      "response": {
        "status_code": 200,
        "headers": {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        },
        "body": {
          "name": "mock-server-go",
          "version": "1.0.0",
          "message": "model example with json file"
        }
      }
    }
  }
}

```

#### Exemplo YML (YAML)

```
---
name: mock-server-go-yaml
version: 1.0.0
actions:
  action_example:
    hold_connection_at_least: 0
    accept_request_method:
      GET: true
      POST: false
    response:
      status_code: 200
      headers:
        Content-Type: text/xml, application/xml
        Access-Control-Allow-Origin: "*"
      body:
        name: "mock-server-go"
        version: "1.0.0"
        message: "model example with yml file"
```

### Adicionando um mock por request

Realizar um request *POST* para a aplicação. Neste caso, 
toda vez que a aplicação for inicializada os mocks adicionados por request
serão inutilizados (apagados) da memória.

```
curl -vL -X POST 'http://{HOST}/' \
--header 'Content-Type: application/json' \
--data-raw '{
  "name": "mock-server-go-json",
  "version": "1.0.0",
  "actions": {
    "action_example": {
      "hold_connection_at_least": 2,
      "accept_request_method": {
        "GET": {
          "available": true
        },
        "POST": {
          "available": false
        }
      },
      "response": {
        "status_code": 200,
        "headers": {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        },
        "body": {
          "name": "mock-server-go",
          "version": "1.0.0",
          "message": "model example with json file"
        }
      }
    },
    "action_ejemplo": {
      "hold_connection_at_least": 5,
      "accept_request_method": {
        "GET": {
          "available": false
        },
        "PUT": {
          "available": true
        }
      },
      "response": {
        "status_code": 201,
        "headers": {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        },
        "body": {
          "name": "mock-server-go",
          "version": "1.0.0",
          "message": "model example with json file"
        }
      }
    }
  }
}'
```

### Funcionalidades

Como funcionalidades por enquanto existem 2 (duas), validação de métodos 
aceitos no request (verbos *POST, GET, PUT,* ...) e segurar o tempo de
resposta da conexão.

- Métodos de requisição aceitos (*accept_request_method*): somente os verbos 
que estiverem como true serão aceitos.

```
...
"accept_request_method":{
    "GET":true,
    "POST":false
}
...
```

- Segurar o tempo de resposta da conexão: segura o tempo de resposta em *x* 
tempo de segundos.

```
...
"hold_connection_at_least":2, // 2 segundos
...
```

#### Exemplo de requisição

```
curl -vL -X GET 'http://{HOST}/mock-server/{MOCK_NAME}/{ACTION_NAME}'
```