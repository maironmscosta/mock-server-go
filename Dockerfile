
# build stage
FROM golang:latest AS builder
# working directory
WORKDIR /app
COPY ./ /app

RUN go get -d /app/cmd/mock-server
# rebuilt built in libraries and disabled cgo
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /app /app/cmd/mock-server
# final stage
FROM alpine:latest
# working directory
WORKDIR /app

# copy the binary file into working directory
COPY --from=builder /app .
# http server listens on port 5000
EXPOSE 5000
# Run the docker_imgs command when the container starts.
CMD ["/app/mock-server"]
