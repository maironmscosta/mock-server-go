package handler

import (
	glog "bitbucket.org/maironmscosta/golang-log/v1"
	"bitbucket.org/maironmscosta/mock-server-go/model"
	"bitbucket.org/maironmscosta/mock-server-go/service"
	"bitbucket.org/maironmscosta/mock-server-go/util"
	"encoding/json"
	"github.com/go-chi/chi"
	"net/http"
)

type Handler struct {
	MockService service.MockServiceInterface
	Logger      glog.Logging
}

const (
	ActionByNameNotFound      = "error to get action by name"
	ErrorBodyResponseAction   = "error to get body from response action"
	ErrorToGetBodyFromRequest = "error to get body from request"
	ErrorAddMock              = "error to add a mock"
	ErrorToReturnAllMocks     = "error to return all mocks"
)

var defaultHeader = map[string]string{"Content-Type": "application/json"}

func (handler *Handler) GetMock(w http.ResponseWriter, r *http.Request) {

	mockName := chi.URLParam(r, "mock_name")
	actionName := chi.URLParam(r, "action_name")

	log := handler.Logger.Base("mockName", mockName, "actionName", actionName)
	action, err := handler.MockService.GetAction(mockName, actionName)
	if err != nil {
		log.Warning(ActionByNameNotFound)
		marshal, _ := json.Marshal(util.ErrorMap(ActionByNameNotFound))
		handler.returning(w, http.StatusBadRequest, defaultHeader, marshal)
		return
	}

	feature := service.NewFeature(log).
		AddAction(action).
		AddRequest(r).
		AddResponseWriter(w)

	if isValid := feature.Execute(); !isValid {
		return
	}

	body, err := action.Response.GetBody()
	if err != nil {
		log.Error(ErrorBodyResponseAction)
		marshal, _ := json.Marshal(util.ErrorMap(ErrorBodyResponseAction))
		handler.returning(w, http.StatusInternalServerError, defaultHeader, marshal)
		return
	}

	log.Info("success to pay the request")
	handler.returning(w, action.Response.StatusCode, action.Response.Headers, body)
}

func (handler *Handler) AddMock(w http.ResponseWriter, r *http.Request) {

	handler.Logger.Info("starting to add a mock")
	var mock model.Mock
	err := json.NewDecoder(r.Body).Decode(&mock)
	if err != nil {
		handler.Logger.Error(ErrorToGetBodyFromRequest)
		marshal, _ := json.Marshal(util.ErrorMap(ErrorToGetBodyFromRequest))
		handler.returning(w, http.StatusBadRequest, defaultHeader, marshal)
		return
	}

	err = handler.MockService.AddMock(mock)
	if err != nil {
		handler.Logger.Error(ErrorAddMock)
		errorMap := util.ErrorMap(ErrorAddMock)
		errorMap["validate"] = err.Error()
		errorMap["mock"] = mock
		marshal, _ := json.Marshal(errorMap)
		handler.returning(w, http.StatusBadRequest, defaultHeader, marshal)
		return
	}

	handler.Logger.Info("success to add a mock")
	handler.returning(w, http.StatusCreated, defaultHeader, nil)

}

func (handler *Handler) GetAllMocks(w http.ResponseWriter, r *http.Request) {

	allMocks := handler.MockService.GetAllMocks()
	mocks, err := json.Marshal(allMocks)
	if err != nil {
		handler.Logger.Error(ErrorToReturnAllMocks)
		marshal, _ := json.Marshal(util.ErrorMap(ErrorToReturnAllMocks))
		handler.returning(w, http.StatusInternalServerError, defaultHeader, marshal)
		return
	}

	handler.returning(w, http.StatusOK, defaultHeader, mocks)

}

func (handler *Handler) returning(w http.ResponseWriter, statusCode int, headers map[string]string, body []byte) {

	if len(headers) != 0 {
		for key, value := range headers {
			w.Header().Add(key, value)
		}
	}

	w.WriteHeader(statusCode)
	w.Write(body)

}
