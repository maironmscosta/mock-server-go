package handler

import (
	glog "bitbucket.org/maironmscosta/golang-log/v1"
	"bitbucket.org/maironmscosta/mock-server-go/model"
	"bitbucket.org/maironmscosta/mock-server-go/service"
	"bytes"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var logger *log.Logger

func TestHandler_GetMock(t *testing.T) {

	tests := []struct {
		Name                   string
		HTTPMethod             string
		ExpectedHttpStatusCode int
		ExpectedHeaders        map[string]string
		ExpectBody             map[string]any
		MockStub               service.MockStub
	}{
		{
			Name:                   "success - must return status code 200 and header content-type for json",
			HTTPMethod:             http.MethodGet,
			ExpectedHttpStatusCode: http.StatusOK,
			ExpectedHeaders: map[string]string{
				"Content-Type": "application/json",
			},
			ExpectBody: map[string]any{
				"name":    "mock-server-go",
				"version": "1.0.0",
				"message": "model example with json file",
			},
			MockStub: service.MockStub{
				ValidateAcceptRequestMethodStub: func(action model.Action, r *http.Request) bool {
					return true
				},
				GetActionStub: func(mockName, actionName string) (model.Action, error) {
					return model.Action{
						AcceptRequestMethod: map[string]map[string]any{
							"GET": {
								"available": true,
							},
						},
						Response: model.Response{
							StatusCode: 200,
							Headers: map[string]string{
								"Content-Type":                "application/json",
								"Access-Control-Allow-Origin": "*",
							},
							Body: map[string]any{
								"name":    "mock-server-go",
								"version": "1.0.0",
								"message": "model example with json file",
							},
						},
					}, nil
				},
			},
		},
		{
			Name:                   "error - must return status code 405 - failed caused by method not allowed",
			HTTPMethod:             http.MethodGet,
			ExpectedHttpStatusCode: http.StatusMethodNotAllowed,
			ExpectedHeaders:        nil,
			ExpectBody:             nil,
			MockStub: service.MockStub{
				ValidateAcceptRequestMethodStub: func(action model.Action, r *http.Request) bool {
					return false
				},
				GetActionStub: func(mockName, actionName string) (model.Action, error) {
					return model.Action{}, nil
				},
			},
		},
		{
			Name:                   "error - must return status code 400 - action not found by name",
			HTTPMethod:             http.MethodGet,
			ExpectedHttpStatusCode: http.StatusBadRequest,
			ExpectedHeaders: map[string]string{
				"Content-Type": "application/json",
			},
			ExpectBody: map[string]any{
				"error": ActionByNameNotFound,
			},
			MockStub: service.MockStub{
				GetActionStub: func(mockName, actionName string) (model.Action, error) {
					return model.Action{}, errors.New(ActionByNameNotFound)
				},
			},
		},
	}

	for _, test := range tests {

		logger = log.New(os.Stdout, "", log.Flags())
		logging := glog.NewLogging(logger)
		handler := &Handler{
			MockService: &test.MockStub,
			Logger:      logging,
		}

		t.Run(test.Name, func(t *testing.T) {

			req := httptest.NewRequest(test.HTTPMethod, "https://teste.com.br", nil)
			w := httptest.NewRecorder()

			handler.GetMock(w, req)
			res := w.Result()
			defer res.Body.Close()

			assert.Equal(t, test.ExpectedHttpStatusCode, res.StatusCode)
			for key, value := range test.ExpectedHeaders {
				assert.Equal(t, value, res.Header.Get(key))
			}

			var body map[string]any
			if res.StatusCode != http.StatusMethodNotAllowed {
				err := json.NewDecoder(res.Body).Decode(&body)
				if err != nil {
					t.Errorf("expected error to be nil got %v", err)
				}
			}

			assert.Equal(t, test.ExpectBody, body)
		})

	}
}

func TestHandler_AddMock(t *testing.T) {

	tests := []struct {
		Name                   string
		Mock                   model.Mock
		MockService            service.MockService
		ExpectedHttpStatusCode int
		ExpectedHeaders        map[string]string
		ExpectedBody           map[string]any
	}{
		{
			Name: "success - must return status code response 201",
			Mock: model.Mock{
				Name: "teste_mock_01",
				Actions: map[string]model.Action{
					"action_01": {
						Response: model.Response{
							StatusCode: 200,
						},
					},
				},
			},
			ExpectedHttpStatusCode: 201,
			ExpectedHeaders: map[string]string{
				"Content-Type": "application/json",
			},
			ExpectedBody: nil,
		},
		{
			Name: "error - must return status code response 400",
			Mock: model.Mock{
				Actions: map[string]model.Action{
					"action_01": {
						Response: model.Response{
							StatusCode: 200,
						},
					},
				},
			},
			ExpectedHttpStatusCode: 400,
			ExpectedHeaders: map[string]string{
				"Content-Type": "application/json",
			},
			ExpectedBody: map[string]any{
				"error":    ErrorAddMock,
				"validate": service.ErrorAddMockNoNameForMock,
				"mock": model.Mock{
					Actions: map[string]model.Action{
						"action_01": {
							Response: model.Response{
								StatusCode: 200,
							},
						},
					},
				},
			},
		},
	}

	for _, test := range tests {

		logger = log.New(os.Stdout, "", log.Flags())
		logging := glog.NewLogging(logger)
		handler := &Handler{
			MockService: &test.MockService,
			Logger:      logging,
		}

		t.Run(test.Name, func(t *testing.T) {

			var buf bytes.Buffer
			_ = json.NewEncoder(&buf).Encode(test.Mock)

			req := httptest.NewRequest(http.MethodPost, "https://teste.com.br", &buf)
			w := httptest.NewRecorder()

			handler.AddMock(w, req)
			res := w.Result()
			defer res.Body.Close()
			data, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Errorf("expected error to be nil got %v", err)
			}

			assert.Equal(t, test.ExpectedHttpStatusCode, res.StatusCode)
			for key, value := range test.ExpectedHeaders {
				assert.Equal(t, value, res.Header.Get(key))
			}

			var body map[string]any
			json.Unmarshal(data, &body)

			marshal, _ := json.Marshal(test.ExpectedBody)
			var expectedBody map[string]any
			json.Unmarshal(marshal, &expectedBody)
			assert.Equal(t, expectedBody, body)

		})
	}
}
