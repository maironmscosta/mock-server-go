package util

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
)

func GetFile (pathFile string, filename string) []byte {

	//filename := path.Join(path.Join(os.Getenv("PWD"), "resource"), "layout.html.mustache")
	filename = path.Join(pathFile, filename)
	file, err := ioutil.ReadFile(filename)

	if err != nil {
		fmt.Println("Error: ", err.Error())
	}

	return file
}

func GetMockPath() string {
	path, _ := os.Getwd()
	path = fmt.Sprintf("%s%s", path, "/mocks")
	return path
}