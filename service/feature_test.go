package service

import (
	glog "bitbucket.org/maironmscosta/golang-log/v1"
	"bitbucket.org/maironmscosta/mock-server-go/model"
	"github.com/stretchr/testify/assert"
	"log"
	"math"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

var logger *log.Logger

func TestFeature_ValidateAcceptRequestMethod(t *testing.T) {

	tests := []struct {
		Name                   string
		MockAction             model.Action
		RequestVerbMethod      string
		ExpectResultValid      bool
		ExpectResultStatusCode int
	}{
		{
			Name: "Get Verb is in the list and it is an acceptable method - must return true",
			MockAction: model.Action{
				AcceptRequestMethod: map[string]map[string]any{
					"GET": {
						"available": true,
					},
				},
			},
			RequestVerbMethod:      http.MethodGet,
			ExpectResultValid:      true,
			ExpectResultStatusCode: http.StatusOK,
		}, {
			Name: "Post Verb is in the list and its not an acceptable method - must return false",
			MockAction: model.Action{
				AcceptRequestMethod: map[string]map[string]any{
					"Post": {
						"available": false,
					},
				},
			},
			RequestVerbMethod:      http.MethodPost,
			ExpectResultValid:      false,
			ExpectResultStatusCode: http.StatusMethodNotAllowed,
		}, {
			Name: "Head Verb is not in the list and its not an acceptable method - must return false",
			MockAction: model.Action{
				AcceptRequestMethod: map[string]map[string]any{
					"Post": {
						"available": true,
					},
					"GET": {
						"available": true,
					},
				},
			},
			RequestVerbMethod:      http.MethodHead,
			ExpectResultValid:      false,
			ExpectResultStatusCode: http.StatusMethodNotAllowed,
		},
	}

	for _, test := range tests {

		logger = log.New(os.Stdout, "", log.Flags())
		logging := glog.NewLogging(logger)

		t.Run(test.Name, func(t *testing.T) {

			request, _ := http.NewRequest(test.RequestVerbMethod, "http:localhost:5000", nil)
			w := httptest.NewRecorder()
			feature := NewFeature(logging).
				AddRequest(request).
				AddResponseWriter(w).
				AddAction(test.MockAction)

			acceptRequestMethod := feature.ValidateAcceptRequestMethod()

			assert.Equal(t, test.ExpectResultValid, acceptRequestMethod)
			assert.Equal(t, test.ExpectResultStatusCode, w.Code)

		})
	}

}

func TestMockService_HoldConnectionAtLeast(t *testing.T) {

	tests := []struct {
		Name       string
		MockAction model.Action
	}{
		{
			Name: "hold connection at least 2 sec",
			MockAction: model.Action{
				HoldConnectionAtLeast: 2,
			},
		},
	}

	for _, test := range tests {

		logger = log.New(os.Stdout, "", log.Flags())
		logging := glog.NewLogging(logger)
		t.Run(test.Name, func(t *testing.T) {

			request, _ := http.NewRequest("GET", "http:localhost:5000", nil)
			w := httptest.NewRecorder()

			feature := NewFeature(logging).
				AddRequest(request).
				AddResponseWriter(w).
				AddAction(test.MockAction)

			now := time.Now()
			feature.HoldConnectionAtLeast()
			since := time.Since(now)
			holdFor := time.Duration(test.MockAction.HoldConnectionAtLeast) * time.Second

			assert.Equal(t, holdFor.Seconds(), math.Trunc(since.Seconds()))

		})
	}

}
