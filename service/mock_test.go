package service

import (
	"bitbucket.org/maironmscosta/mock-server-go/model"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMockService_GetMock(t *testing.T) {

	tests := []struct {
		Name           string
		MockName       string
		Mock           model.Mock
		ExpectedResult model.Mock
	}{
		{
			Name:     "exists mock with name action",
			MockName: "action",
			Mock: model.Mock{
				Actions: map[string]model.Action{
					"action": {
						AcceptRequestMethod: map[string]map[string]any{
							"POST": {
								"available": true,
							},
						},
					},
				},
			},
			ExpectedResult: model.Mock{
				Actions: map[string]model.Action{
					"action": {
						AcceptRequestMethod: map[string]map[string]any{
							"POST": {
								"available": true,
							},
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			mockService := MockService{
				MocksLoaded: map[string]model.Mock{
					test.MockName: test.Mock,
				},
			}
			mock, _ := mockService.GetMock(test.MockName)
			assert.Equal(t, test.ExpectedResult, mock)
		})
	}
}

func TestMockService_GetAction(t *testing.T) {

	tests := []struct {
		Name           string
		MockName       string
		ActionName     string
		MockLoaded     map[string]model.Mock
		ExpectedAction model.Action
		ExpectedError  error
	}{
		{
			Name:       "success - exists action when search for",
			MockName:   "mock_01",
			ActionName: "action_01",
			MockLoaded: map[string]model.Mock{
				"mock_01": {
					Name: "mock_01",
					Actions: map[string]model.Action{
						"action_01": {
							HoldConnectionAtLeast: 2,
						},
					},
				},
			},
			ExpectedAction: model.Action{
				HoldConnectionAtLeast: 2,
			},
			ExpectedError: nil,
		},
		{
			Name:       "error - not exists action when search for",
			MockName:   "mock_01",
			ActionName: "action_not_found",
			MockLoaded: map[string]model.Mock{
				"mock_01": {
					Name: "mock_01",
					Actions: map[string]model.Action{
						"action_01": {
							HoldConnectionAtLeast: 2,
						},
					},
				},
			},
			ExpectedAction: model.Action{},
			ExpectedError:  errors.New(ActionNotFound),
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			mockService := MockService{
				MocksLoaded: test.MockLoaded,
			}

			action, err := mockService.GetAction(test.MockName, test.ActionName)
			if err != nil {
				assert.Equal(t, test.ExpectedError.Error(), err.Error())
			}
			assert.Equal(t, test.ExpectedAction, action)
		})
	}
}

func TestMockService_AddMock(t *testing.T) {

	tests := []struct {
		Name                 string
		Mock                 model.Mock
		ExpectedMapMockAdded map[string]model.Mock
		ExpectedError        error
	}{
		{
			Name: "success - mock was added successfully",
			Mock: model.Mock{
				Name: "mock_teste_01",
				Actions: map[string]model.Action{
					"action_01": {
						Response: model.Response{
							StatusCode: 200,
						},
					},
				},
			},
			ExpectedMapMockAdded: map[string]model.Mock{
				"mock_teste_01": {
					Name: "mock_teste_01",
					Actions: map[string]model.Action{
						"action_01": {
							Response: model.Response{
								StatusCode: 200,
							},
						},
					},
				},
			},
			ExpectedError: nil,
		},
		{
			Name: "error - mock was not added - there is no name for mock",
			Mock: model.Mock{
				Actions: map[string]model.Action{
					"action_01": {
						Response: model.Response{
							StatusCode: 200,
						},
					},
				},
			},
			ExpectedMapMockAdded: nil,
			ExpectedError:        errors.New(ErrorAddMockNoNameForMock),
		},
		{
			Name: "error - mock was not added - there is no action for mock",
			Mock: model.Mock{
				Name: "mock_teste_01",
			},
			ExpectedMapMockAdded: nil,
			ExpectedError:        errors.New(ErrorAddMockNoActionForMock),
		},
		{
			Name:                 "error - mock was not added - there is no name and no action for mock",
			Mock:                 model.Mock{},
			ExpectedMapMockAdded: nil,
			ExpectedError:        errors.New(fmt.Sprintf("%s,%s", ErrorAddMockNoNameForMock, ErrorAddMockNoActionForMock)),
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			mockService := MockService{}
			err := mockService.AddMock(test.Mock)
			if err != nil {
				assert.Equal(t, test.ExpectedError.Error(), err.Error())
			}

			assert.Equal(t, test.ExpectedMapMockAdded, mockService.MocksAdded)

		})
	}
}
