package service

import (
	glog "bitbucket.org/maironmscosta/golang-log/v1"
	"bitbucket.org/maironmscosta/mock-server-go/model"
	"net/http"
	"strings"
	"time"
)

const (
	MethodNotAllowed = "method is not valid"
)

type Feature struct {
	Logger  glog.Logging
	action  model.Action
	request *http.Request
	w       http.ResponseWriter
}

type FeatureInterface interface {
	AddAction(action model.Action) Feature
	AddRequest(request *http.Request) Feature
	AddResponseWriter(w http.ResponseWriter) Feature
	ValidateAcceptRequestMethod(action model.Action, r *http.Request) error
	HoldConnectionAtLeast()
}

func (feature Feature) AddAction(action model.Action) Feature {
	feature.action = action
	return feature
}

func (feature Feature) AddRequest(request *http.Request) Feature {
	feature.request = request
	return feature
}

func (feature Feature) AddResponseWriter(w http.ResponseWriter) Feature {
	feature.w = w
	return feature
}

func NewFeature(logger glog.Logging) Feature {
	return Feature{
		Logger: logger,
	}
}

func (feature Feature) Execute() bool {

	if !feature.ValidateAcceptRequestMethod() {
		return false
	}

	feature.HoldConnectionAtLeast()

	return true
}

func (feature Feature) ValidateAcceptRequestMethod() bool {

	if len(feature.action.AcceptRequestMethod) != 0 {
		for key, value := range feature.action.AcceptRequestMethod {
			available := value["available"].(bool)
			if strings.ToUpper(key) == strings.ToUpper(feature.request.Method) && available {
				return true
			}
		}
	}

	feature.Logger.Warning("execute feature validation", "validate accept request method", MethodNotAllowed)
	feature.w.WriteHeader(http.StatusMethodNotAllowed)

	return false
}

func (feature Feature) HoldConnectionAtLeast() {
	time.Sleep(time.Duration(feature.action.HoldConnectionAtLeast) * time.Second)
}
