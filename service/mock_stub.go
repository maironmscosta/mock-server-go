package service

import (
	glog "bitbucket.org/maironmscosta/golang-log/v1"
	"bitbucket.org/maironmscosta/mock-server-go/model"
	"errors"
	"net/http"
	"time"
)

type MockStub struct {
	NewMockServiceStub              func(Logger glog.Logging, refreshLoad time.Duration) *MockService
	GetMockStub                     func(key string) (model.Mock, error)
	GetAllMocksStub                 func() []model.Mock
	AddMockStub                     func(mock model.Mock) error
	GetActionStub                   func(mockName, actionName string) (model.Action, error)
	ValidateAcceptRequestMethodStub func(action model.Action, r *http.Request) bool
	HoldConnectionAtLeastStub       func(holdFor int)
}

func (stub *MockStub) GetAllMocks() []model.Mock {

	if stub.GetAllMocksStub() != nil {
		return stub.GetAllMocksStub()
	}
	return []model.Mock{}
}

func (stub *MockStub) AddMock(mock model.Mock) error {
	if stub.AddMockStub != nil {
		return stub.AddMockStub(mock)
	}
	return errors.New("")
}

func (stub *MockStub) GetMock(key string) (model.Mock, error) {
	if stub.GetMockStub != nil {
		return stub.GetMockStub(key)
	}
	return model.Mock{}, nil
}

func (stub *MockStub) GetAction(mockName, actionName string) (model.Action, error) {
	if stub.GetActionStub != nil {
		return stub.GetActionStub(mockName, actionName)
	}
	return model.Action{}, nil
}

func (stub *MockStub) ValidateAcceptRequestMethod(action model.Action, r *http.Request) bool {
	if stub.ValidateAcceptRequestMethodStub != nil {
		return stub.ValidateAcceptRequestMethodStub(action, r)
	}
	return false
}

func (stub *MockStub) HoldConnectionAtLeast(holdFor int) {
	if stub.HoldConnectionAtLeastStub != nil {
		stub.HoldConnectionAtLeastStub(holdFor)
	}
}
