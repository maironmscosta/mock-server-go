package service

import (
	glog "bitbucket.org/maironmscosta/golang-log/v1"
	"bitbucket.org/maironmscosta/mock-server-go/model"
	"bitbucket.org/maironmscosta/mock-server-go/util"
	"encoding/json"
	"errors"
	"fmt"
	yaml "gopkg.in/yaml.v2"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

const (
	ActionNotFound              = "action not found into mock"
	ErrorAddMockNoNameForMock   = "there is no name for mock"
	ErrorAddMockNoActionForMock = "there is no actions for mock"
	ErrorRemoveMockLoaded       = "it cannot remove a mock loaded"
)

var mutex = &sync.Mutex{}

type MockService struct {
	Logger      glog.Logging
	MocksLoaded map[string]model.Mock
	MocksAdded  map[string]model.Mock
}

type MockServiceInterface interface {
	GetMock(key string) (model.Mock, error)
	GetAllMocks() []model.Mock
	AddMock(mock model.Mock) error
	GetAction(mockName, actionName string) (model.Action, error)
}

func NewMockService(Logger glog.Logging, refreshLoad time.Duration) *MockService {
	m := &MockService{
		Logger: Logger,
	}

	m.initLoad(refreshLoad)
	return m
}

func (service *MockService) initLoad(refreshLoad time.Duration) {

	service.load()
	go func() {
		ticker := time.NewTicker(refreshLoad)
		for _ = range ticker.C {
			service.load()
		}
	}()
}

func (service *MockService) load() {

	service.Logger.Info("starting loading mocks")

	mockPath := util.GetMockPath()
	entries, err := os.ReadDir(mockPath)
	if err != nil {
		service.Logger.Warning("error to get mocks entries", "mockPath", mockPath)
		return
	}

	mockMap := make(map[string]model.Mock, len(entries))
	for _, entry := range entries {

		filePath := fmt.Sprintf("%s/%s", mockPath, entry.Name())
		file, err := os.ReadFile(filePath)
		if err != nil {
			service.Logger.Warning("cannot load file", "file", filePath)
			continue
		}

		var mock model.Mock
		switch filepath.Ext(entry.Name()) {
		case ".json":
			err = json.Unmarshal(file, &mock)
			if err != nil {
				service.Logger.Warning("cannot unmarshal json file to mock", "entry", entry.Name())
				continue
			}
		case ".yml":
			err := yaml.Unmarshal(file, &mock)
			if err != nil {
				service.Logger.Warning("cannot unmarshal yaml/yml file to mock", "entry", entry.Name())
				continue
			}
		case ".yaml":
			err := yaml.Unmarshal(file, &mock)
			if err != nil {
				service.Logger.Warning("cannot unmarshal yaml/yml file to mock", "entry", entry.Name())
				continue
			}
		default:
			service.Logger.Warning("invalid file to mock", "entry", entry.Name())
			continue
		}

		if len(mock.Name) > 0 {
			mockMap[mock.Name] = mock
		}
	}

	if len(mockMap) > 0 {
		service.MocksLoaded = mockMap
		service.Logger.Info("success to refresh mocks")
	}

	service.Logger.Info("finish to refresh mocks loaded", "mocks", service.MocksLoaded)
}

func (service *MockService) GetMock(key string) (model.Mock, error) {

	if mock, exists := service.MocksAdded[key]; exists {
		return mock, nil
	}

	if mock, exists := service.MocksLoaded[key]; exists {
		return mock, nil
	}

	return model.Mock{}, errors.New("error to get mock by name")
}

func (service *MockService) GetAllMocks() (mocks []model.Mock) {

	mocks = make([]model.Mock, 0)
	for _, value := range service.MocksAdded {
		mocks = append(mocks, value)
	}

	for _, value := range service.MocksLoaded {
		mocks = append(mocks, value)
	}

	return
}

func (service *MockService) GetAction(mockName, actionName string) (model.Action, error) {

	mutex.Lock()
	defer mutex.Unlock()
	mock, err := service.GetMock(mockName)
	if err != nil {
		return model.Action{}, err
	}

	action, exists := mock.Actions[actionName]
	if !exists {
		return model.Action{}, errors.New(ActionNotFound)
	}

	return action, nil
}

func (service *MockService) AddMock(mock model.Mock) error {

	errs := make([]string, 0)
	if len(mock.Name) == 0 {
		errs = append(errs, ErrorAddMockNoNameForMock)
	}

	if len(mock.Actions) == 0 {
		errs = append(errs, ErrorAddMockNoActionForMock)
	}

	if _, found := service.MocksLoaded[mock.Name]; found {
		errs = append(errs, ErrorRemoveMockLoaded)
	}

	if len(errs) > 0 {
		return errors.New(strings.Join(errs[:], ","))
	}

	if service.MocksAdded == nil {
		service.MocksAdded = make(map[string]model.Mock, 0)
	}

	mutex.Lock()
	delete(service.MocksAdded, mock.Name)
	service.MocksAdded[mock.Name] = mock
	mutex.Unlock()

	return nil
}
