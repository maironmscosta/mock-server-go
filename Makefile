#makefile

APPLICATION_NAME := mock-server
PORT := 5000

build:
	docker build -t $(APPLICATION_NAME):latest .

build-run:	build
	docker run -p $(PORT):$(PORT) -t $(APPLICATION_NAME):latest

run:
	docker run -p $(PORT):$(PORT) -t $(APPLICATION_NAME):latest
