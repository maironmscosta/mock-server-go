package main

import (
	glog "bitbucket.org/maironmscosta/golang-log/v1"
	"bitbucket.org/maironmscosta/mock-server-go/environment"
	"bitbucket.org/maironmscosta/mock-server-go/handler"
	"bitbucket.org/maironmscosta/mock-server-go/service"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/kelseyhightower/envconfig"
	"log"
	"net/http"
	"os"
	"time"
)

var logger *log.Logger

func init() {

	err := envconfig.Process("setting", &environment.Setting)
	if err != nil {
		panic(err.Error())
	}

	logger = log.New(os.Stdout, "", log.Flags())

}

func main() {

	logging := glog.NewLogging(logger)

	mockService := service.NewMockService(logging, environment.Setting.MockService.RefreshInterval)
	handler := &handler.Handler{
		MockService: mockService,
		Logger:      logging,
	}

	router := chi.NewRouter()
	context := environment.Setting.Server.Context

	router.Route(fmt.Sprintf("/%s", context), func(router chi.Router) {
		router.HandleFunc(fmt.Sprintf("/{mock_name}/{action_name}"), handler.GetMock)
		router.Post(fmt.Sprintf("/"), handler.AddMock)
		router.Get(fmt.Sprintf("/"), handler.GetAllMocks)
	})

	serverHttp := &http.Server{
		Addr:           fmt.Sprintf(":%s", environment.Setting.Server.Port),
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	fmt.Println("Server started in", "Port: ", serverHttp.Addr, "Context: ", context)
	if err := serverHttp.ListenAndServe(); err != nil {
		fmt.Println("Listen and Serve", "err", err.Error())
		panic(err.Error())
	}
}
