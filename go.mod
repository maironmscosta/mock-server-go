module bitbucket.org/maironmscosta/mock-server-go

go 1.19

require (
	bitbucket.org/maironmscosta/golang-log v0.0.0-20210718030415-3972f50590aa
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v2 v2.3.0
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
